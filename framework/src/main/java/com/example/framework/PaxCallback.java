package com.example.framework;

import android.content.Context;

import com.android.volley.VolleyError;
import com.example.framework.service.PaxBaseRequest;
import com.example.framework.service.connector.PaxConnector;
import com.example.framework.utils.PaxUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bruno on 16/10/17.
 */

public abstract class PaxCallback<T> extends PaxBaseRequest {

    private Class mClassResponse;
    private Context mContext;

    public abstract void onSuccess(T response);

    public abstract void onError(String error);

    public void executeRequest(PaxConnector connector) {
        mContext = connector.getContext();
        mClassResponse = connector.getClassResponse();

        executeVolleyRequest(mContext, connector);
    }

    @Override
    public void onResponse(String response) {
        //colocar breakpoint aq

        try {
            onSuccess((T) new PaxUtils().doParse(mClassResponse, response));

        } catch (Exception e) {
            onError(ERROR_PARSE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        String errorMessage = CONNECTIVITY_ERROR;

        if (PaxUtils.isNetworkConnected(mContext)) {
            errorMessage = NON_AVAILABLE_ERROR;
        }

        onError(errorMessage);
    }
}
