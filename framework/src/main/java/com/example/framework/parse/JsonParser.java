package com.example.framework.parse;

import java.io.IOException;

/**
 * Created by ecpereira - Ewerton Cavalcante on 01/02/17.
 * Stefanini
 */

public class JsonParser<T> extends RootUnwrappedParser {

    private Class<T> mClassType;

    public JsonParser(Class<T> classType) {
        this.mClassType = classType;
    }

    public T parse(String jsonContent) throws IOException {
        return OBJECT_MAPPER.readValue(jsonContent, mClassType);
    }

}
