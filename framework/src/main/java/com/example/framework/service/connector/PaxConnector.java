package com.example.framework.service.connector;

import android.content.Context;

import java.util.Map;

/**
 * Created by bruno on 23/10/17.
 */

public class PaxConnector {

    public enum HttpRequestType{
        RETROFIT2, VOLLEY
    }

    public enum MethodType {
        GET, PUT, DELETE, POST
    }

    private Context context;

    private Object body;

    private Class classResponse;

    private String url;

    private Map<String, String> headers;

    private HttpRequestType requestType;

    private MethodType methodType;

    public PaxConnector(Builder builder) {
        context = builder.context;
        body = builder.body;
        classResponse = builder.classResponse;
        url = builder.url;
        headers = builder.headers;
        requestType = builder.requestType;
        methodType = builder.methodType;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public Class getClassResponse() {
        return classResponse;
    }

    public void setClassResponse(Class classResponse) {
        this.classResponse = classResponse;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public HttpRequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(HttpRequestType requestType) {
        this.requestType = requestType;
    }

    public MethodType getMethodType() {
        return methodType;
    }

    public void setMethodType(MethodType methodType) {
        this.methodType = methodType;
    }

    public static class Builder{
        private Context context;

        private Object body;

        private Class classResponse;

        private String url;

        private Map<String, String> headers;

        private HttpRequestType requestType;

        private MethodType methodType;

        public Builder context(Context context){
            this.context = context;
            return this;
        }

        public Builder httpRequestType(HttpRequestType requestType){
            this.requestType = requestType;
            return this;
        }

        public Builder body(Object body) {
            this.body = body;
            return this;
        }

        public Builder classResponse(Class classResponse) {
            this.classResponse = classResponse;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder headers(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        public Builder methodType(MethodType methodType) {
            this.methodType = methodType;
            return this;
        }

        public PaxConnector load() {
            return new PaxConnector(this);
        }
    }
}
