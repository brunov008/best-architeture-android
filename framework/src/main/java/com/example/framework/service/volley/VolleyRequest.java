package com.example.framework.service.volley;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.framework.service.RequestInterface;
import com.example.framework.service.PaxBaseRequest;
import com.example.framework.service.connector.PaxConnector;
import com.example.framework.utils.PaxUtils;

/**
 * Created by bruno on 23/10/17.
 */

public abstract class VolleyRequest extends PaxBaseRequest implements Response.Listener<String>, Response.ErrorListener {

    private Context mContext;
    private RequestInterface requestListener;
    private Class mClassResponse;

    public VolleyRequest(PaxConnector params) {
        mContext = params.getContext();
        mClassResponse = params.getClassResponse();

        CustomStringRequest request = new CustomStringRequest(getMethod(params.getMethodType()), params.getUrl(), this, this);
        request.setBodyAndHeaders(params.getHeaders(), params.getBody());

        execute(request);
    }

    @Override
    public void onResponse(String response) {
        try {
            requestListener.onSuccess(new PaxUtils().doParse(mClassResponse, response));
        } catch (Exception e) {
            requestListener.onError(ERROR_PARSE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        String errorMessage = CONNECTIVITY_ERROR;

        if (PaxUtils.isNetworkConnected(mContext)) {
            errorMessage = NON_AVAILABLE_ERROR;
        }

        requestListener.onError(errorMessage);
    }

    private void execute(Request request) {

        request.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(mContext).add(request);
    }

    public void setRequestListener(RequestInterface requestInterface) {
        this.requestListener = requestInterface;
    }
}
