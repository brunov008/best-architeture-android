package com.example.framework.service;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.framework.service.connector.PaxConnector;
import com.example.framework.service.volley.CustomStringRequest;

import static com.android.volley.Request.Method.DELETE;
import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.android.volley.Request.Method.PUT;

/**
 * Created by bruno on 18/10/17.
 */

public abstract class PaxBaseRequest implements Response.Listener<String>, Response.ErrorListener{

    protected final static int DEFAULT_TIMEOUT = 30000;

    protected final static String CONNECTIVITY_ERROR = "Não há conexão com a internet";

    protected final static String NON_AVAILABLE_ERROR = "Houve uma falha de conexão. Tente mais tarde.";

    protected final static String ERROR_PARSE = "Erro na leitura dos dados do servidor.";

    protected void executeVolleyRequest(Context context, PaxConnector connector) {

        CustomStringRequest request = new CustomStringRequest(getMethod(connector.getMethodType()), connector.getUrl(), this, this);
        request.setBodyAndHeaders(connector.getHeaders(), connector.getBody());

        request.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(context).add(request);
    }

    protected int getMethod(PaxConnector.MethodType methodType) {
        int typeMethod;

        switch (methodType) {
            case GET:
                typeMethod = GET;
                break;
            case POST:
                typeMethod = POST;
                break;
            case PUT:
                typeMethod = PUT;
                break;
            case DELETE:
                typeMethod = DELETE;
                break;
            default:
                typeMethod = GET;
                break;
        }

        return typeMethod;
    }
}
