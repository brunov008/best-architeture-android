package com.example.framework.service;

/**
 * Created by ecpereira - Ewerton Cavalcante on 18/01/17.
 * Stefanini
 */

public class ResponseResult<T> {
    private T mResult;

    private String mSuccessMessage;

    public ResponseResult(T result) {
        mResult = result;
    }

    public ResponseResult(String successMessage) {
        mSuccessMessage = successMessage;
    }

    public T getResult() {
        return mResult;
    }

    public void setResult(T result) {
        mResult = result;
    }

    public String getSuccessMessage() {
        return mSuccessMessage;
    }

    public void setSuccessMessage(String successMessage) {
        mSuccessMessage = successMessage;
    }
}
