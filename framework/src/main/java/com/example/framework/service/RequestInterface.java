package com.example.framework.service;

/**
 * Created by bruno on 23/10/17.
 */

public interface RequestInterface<T> {

    void onSuccess(T response);

    void onError(String error);
}
