package com.example.framework.service.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bruno on 17/10/17.
 */

public class CustomStringRequest extends StringRequest {

    private static final String TAG = CustomStringRequest.class.getName();

    private static final String DEFAULT_RESPONSE_ENCODING = "UTF-8";

    private static final String DEFAULT_HEADER_KEY = "Content-Type";

    private static final String DEFAULT_HEADER_VALUE = "application/json";

    private static final String VALUE_CONTENT_TYPE_UTF8 = "application/json; charset=utf-8";

    private Map<String, String> headers;
    private Object body;

    public CustomStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public void setBodyAndHeaders(Map<String, String> headers, Object body){
        this.headers = headers;
        this.body = body;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        try {
            if (response.data != null && response.headers != null) {
                String parsed = new String(response.data, DEFAULT_RESPONSE_ENCODING);
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {

        Object bodyParams = body;

        if (bodyParams != null) {

            if (bodyParams instanceof String) {
                return ((String) bodyParams).getBytes();
            } else {
                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    String stringMapper = objectMapper.writeValueAsString(bodyParams);
                    return stringMapper.getBytes();
                } catch (JsonProcessingException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
        return null;
    }

    @Override
    public String getBodyContentType() {
        return VALUE_CONTENT_TYPE_UTF8;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> emptyHeaders = new HashMap<>();
        emptyHeaders.put(DEFAULT_HEADER_KEY, DEFAULT_HEADER_VALUE);

        if (headers != null){
            return headers;
        }
        return emptyHeaders;
    }
}
