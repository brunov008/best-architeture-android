package com.example.framework.utils.activityutil;

import android.content.Context;
import android.content.Intent;

import com.example.framework.utils.activityutil.listener.OnActivityResultListener;

/**
 * Created by bruno on 05/12/17.
 */

public class ActivityResultUtil {

    public static Builder build(Context context) {
        return new Builder(context);
    }


    public static class Builder {

        private Context context;
        private OnActivityResultListener listener;
        private Intent intent;

        public Builder(Context context) {
            this.context = context;
        }

        public void startActivityForResult() {
            ProxyActivity.startActivityForResult(context, intent, listener);
        }

        public Builder setListener(OnActivityResultListener listener) {
            this.listener = listener;
            return this;
        }

        public Builder setIntent(Intent intent) {
            this.intent = intent;
            return this;
        }

    }
}
