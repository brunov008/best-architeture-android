package com.example.framework.utils.activityutil.listener;

import android.content.Intent;

/**
 * Created by bruno on 05/12/17.
 */

public interface OnActivityResultListener {

    void onActivityResult(int resultCode, Intent data);
}
