package com.example.framework.utils.activityutil.model;

import android.content.Intent;

import com.example.framework.utils.activityutil.listener.OnActivityResultListener;

/**
 * Created by bruno on 05/12/17.
 */

public class ActivityRequest {

    private Intent intent;
    private OnActivityResultListener listener;

    public ActivityRequest(Intent intent,
                           OnActivityResultListener listener) {
        this.intent = intent;
        this.listener = listener;
    }

    public Intent getIntent() {
        return intent;
    }

    public OnActivityResultListener getListener() {
        return listener;
    }
}
