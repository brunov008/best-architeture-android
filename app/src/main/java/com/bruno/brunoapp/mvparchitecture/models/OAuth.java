package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 29/11/17.
 */

public class OAuth extends BaseModel {

    @JsonProperty("tokenBackEnd")
    private TokenBackEnd mTokenBackEnd;

    @JsonProperty("codigoRetorno")
    private String mReturnCode;

    @JsonProperty("descricaoRetorno")
    private String mReturnDescription;

    public TokenBackEnd getTokenBackEnd() {
        return mTokenBackEnd;
    }

    public void setTokenBackEnd(TokenBackEnd tokenBackEnd) {
        mTokenBackEnd = tokenBackEnd;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnDescription() {
        return mReturnDescription;
    }

    public void setReturnDescription(String returnDescription) {
        mReturnDescription = returnDescription;
    }
}
