package com.bruno.brunoapp.mvparchitecture.mainactivity.dagger;

import android.content.Context;

import com.bruno.brunoapp.mvparchitecture.mainactivity.MainModel;
import com.bruno.brunoapp.mvparchitecture.mainactivity.MainPresenter;
import com.bruno.brunoapp.mvparchitecture.mainactivity.MainView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bruno on 29/11/17.
 */

@Module
public class MainModule {

    private MainView view;

    public MainModule(MainView view) {
        this.view = view;
    }

    @Provides
    MainPresenter providePresenter(MainModel model){
        return new MainPresenter(view, model);
    }

    @Provides
    MainModel provideModel(){
        return new MainModel((Context) view);
    }
}
