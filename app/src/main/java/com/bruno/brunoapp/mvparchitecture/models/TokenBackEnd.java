package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 29/11/17.
 */

public class TokenBackEnd extends BaseModel {

    @JsonProperty("OAuth2AccessToken")
    private OAuthAccessToken mOAuthAccessToken;

    @JsonProperty("OAuth2Exception")
    private OAuth2Exception mOAuth2Exception;

    public OAuth2Exception getOAuth2Exception() {
        return mOAuth2Exception;
    }

    public void setOAuth2Exception(OAuth2Exception OAuth2Exception) {
        mOAuth2Exception = OAuth2Exception;
    }

    public OAuthAccessToken getOAuthAccessToken() {
        return mOAuthAccessToken;
    }

    public void setOAuthAccessToken(OAuthAccessToken OAuthAccessToken) {
        mOAuthAccessToken = OAuthAccessToken;
    }
}
