package com.bruno.brunoapp.mvparchitecture.core.layer.model;

import android.content.Context;

/**
 * Created by bruno on 29/11/17.
 */

public class BaseServiceModel {

    public Context context;

    public BaseServiceModel(Context context) {
        this.context = context;
    }
}
