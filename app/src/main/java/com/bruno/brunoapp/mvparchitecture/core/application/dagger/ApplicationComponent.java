package com.bruno.brunoapp.mvparchitecture.core.application.dagger;

import android.app.Application;
import android.content.Context;

import com.bruno.brunoapp.mvparchitecture.core.application.AppController;
import com.bruno.brunoapp.mvparchitecture.core.data.DataManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by bruno on 11/12/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(AppController demoApplication);

    @ApplicationContext
    Context getContext();

    Application getApplication();

    DataManager getDataManager();
}
