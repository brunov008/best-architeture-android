package com.bruno.brunoapp.mvparchitecture.exampleactivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.bruno.brunoapp.mvparchitecture.R;
import com.bruno.brunoapp.mvparchitecture.core.layer.view.BaseActivity;
import com.bruno.brunoapp.mvparchitecture.mainactivity.MainActivity;
import com.example.framework.utils.activityutil.ActivityResultUtil;

import butterknife.OnClick;

public class ExampleActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
    }

    @OnClick(R.id.button)
    public void onButtonClick(){

        ActivityResultUtil.build(this)
                .setIntent(new Intent(ExampleActivity.this, MainActivity.class))
                .setListener(((resultCode, data) -> {
                    if (resultCode == 2) makeToast("Sucesso");
                }))
                .startActivityForResult();
    }

    private void makeToast(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }
}
