package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

/**
 * Created by Bruno on 22/08/2018.
 */

@JsonRootName("root")
public class AchievementResponse extends BaseModel {

    @JsonProperty("root")
    private List<AchievementNew> listAchievements;

    public List<AchievementNew> getListAchievements() {
        return listAchievements;
    }

    public void setListAchievements(List<AchievementNew> listAchievements) {
        this.listAchievements = listAchievements;
    }
}
