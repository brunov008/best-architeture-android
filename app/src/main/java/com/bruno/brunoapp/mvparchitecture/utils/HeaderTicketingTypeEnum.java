package com.bruno.brunoapp.mvparchitecture.utils;

/**
 * Created by bruno on 29/11/17.
 */

public enum HeaderTicketingTypeEnum {

    LOCATION("location"),
    MODEL("model"),
    VERSION("version"),
    ROOT("isRoot"),
    DEVICE_ID("deviceId"),
    SIM_CARD("simCard"),
    BILLING_ID("billingId"),
    SYSTEM_NAME("systemName"),
    ENVIRONMENT_NAME("environmentName"),
    COMPANY_NAME("companyName"),
    PRODUCT_NAME("productName"),
    APP_VERSION("appVersion"),
    OS_NAME("osName");

    private String mCode;


    HeaderTicketingTypeEnum(String code) {
        mCode = code;
    }

    public String getCode() {
        return mCode;
    }
}
