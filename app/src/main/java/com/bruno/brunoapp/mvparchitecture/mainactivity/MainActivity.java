package com.bruno.brunoapp.mvparchitecture.mainactivity;

import android.os.Bundle;
import android.widget.TextView;

import com.bruno.brunoapp.mvparchitecture.R;
import com.bruno.brunoapp.mvparchitecture.core.Components;
import com.bruno.brunoapp.mvparchitecture.core.data.DataManager;
import com.bruno.brunoapp.mvparchitecture.core.layer.view.BaseActivity;
import com.bruno.brunoapp.mvparchitecture.mainactivity.dagger.MainModule;
import com.bruno.brunoapp.mvparchitecture.models.AchievementNew;
import com.bruno.brunoapp.mvparchitecture.models.OAuth;
import com.bruno.brunoapp.mvparchitecture.models.User;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainView {

    @Inject
    MainPresenter presenter;

    @Inject
    DataManager dataManager;

    @BindView(R.id.text_response)
    TextView tvResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Components.getMainComponent(new MainModule(this), this);

        //dataManager.saveAccessToken("e10adc3949ba59abbe56e057f20f883e");

        presenter.gellAchievements();
    }

    @OnClick(R.id.button_back)
    public void onButtonBackClick(){
        setResult(2);
        finish();
    }

    @Override
    public void onSuccess(OAuth response) {
        tvResponse.setText("description : " + response.getReturnDescription()
                + "\n status : " + response.getReturnCode()
                + "\n token : " + response.getTokenBackEnd().getOAuthAccessToken().getAccessToken()
                + "\n expiryIn : " + response.getTokenBackEnd().getOAuthAccessToken().getExpiresIn()
                + "\n refreshToken : " + response.getTokenBackEnd().getOAuthAccessToken().getRefreshToken());
    }

    @Override
    public void onListDelivered(List<AchievementNew> list) {
        StringBuilder builder = new StringBuilder();
        for (AchievementNew response: list){
            builder.append("idAchievement : " + response.getIdAchievement()
                    + "\n nameAchievement : " + response.getNameAchievement()
                    + "\n descriptionAchievement : " + response.getDescriptionAchievement()
                    + "\n quantity : " + response.getQuantity()
                    + "\n keys : " + response.getKeys());
        }
        tvResponse.setText(builder);
    }
}
