package com.bruno.brunoapp.mvparchitecture.core;

import com.bruno.brunoapp.mvparchitecture.core.application.AppController;
import com.bruno.brunoapp.mvparchitecture.mainactivity.MainActivity;
import com.bruno.brunoapp.mvparchitecture.mainactivity.dagger.DaggerMainComponent;
import com.bruno.brunoapp.mvparchitecture.mainactivity.dagger.MainModule;

/**
 * Created by bruno on 05/12/17.
 */

public class Components {

   public static void getMainComponent(MainModule module, MainActivity activity){
        DaggerMainComponent.builder()
                .mainModule(module)
                .applicationComponent(AppController.getInstance(activity).getComponent())
                .build()
                .inject(activity);
    }
}
