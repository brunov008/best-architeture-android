package com.bruno.brunoapp.mvparchitecture.core.layer.presenter;

import android.content.Context;

import com.bruno.brunoapp.mvparchitecture.core.layer.model.BaseServiceModel;
import com.bruno.brunoapp.mvparchitecture.core.layer.view.BaseView;


/**
 * Created by bruno on 29/11/17.
 */

public class BasePresenter<T extends BaseView, R extends BaseServiceModel> {

    protected T mView;
    protected R mModel;
    protected Context context;

    //Activities
    public BasePresenter(T view, R model) {
        mView =  view;
        mModel = model;
        context = (Context)view;
    }
}
