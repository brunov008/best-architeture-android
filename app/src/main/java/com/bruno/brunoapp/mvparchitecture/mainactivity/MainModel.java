package com.bruno.brunoapp.mvparchitecture.mainactivity;

import android.content.Context;

import com.bruno.brunoapp.mvparchitecture.core.layer.model.BaseServiceModel;
import com.bruno.brunoapp.mvparchitecture.models.AchievementResponse;
import com.bruno.brunoapp.mvparchitecture.models.OAuth;
import com.bruno.brunoapp.mvparchitecture.models.User;
import com.bruno.brunoapp.mvparchitecture.utils.AndroidUtils;
import com.bruno.brunoapp.mvparchitecture.utils.UrlUtils;
import com.example.framework.PaxCallback;
import com.example.framework.service.connector.PaxConnector;

import java.util.HashMap;

/**
 * Created by bruno on 29/11/17.
 */

public class MainModel extends BaseServiceModel {

    private static final String ACHIEVEMENT_EXPLORER_RETRIEVE_REQUEST_URL = "http://missaonascente.000webhostapp.com/Achievement.php"; //Retorna os achievements do explorador

    public MainModel(Context context) {
        super(context);
    }

    public void requestToken(PaxCallback callback, User user){

        HashMap<String, String> params = new HashMap<>();

        params.put("grant_type", "password");
        params.put("username", user.getCpf());
        params.put("password", user.getPassword());

        String bodyParams = UrlUtils.encodeParameters(params, false);

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Authorization", "Basic Z3JlZW5jYXJkLWNsaWVudDpncmVlbmNhcmQtcHJkLXNlY3JldA==");
        headers.put("billingId", user.getCpf());
        AndroidUtils.setTicketing(user, headers, context);

        PaxConnector connector = new PaxConnector.Builder()
                .context(context)
                .body(bodyParams)
                .url(ACHIEVEMENT_EXPLORER_RETRIEVE_REQUEST_URL)
                .classResponse(OAuth.class)
                .headers(headers)
                .methodType(PaxConnector.MethodType.POST)
                .httpRequestType(PaxConnector.HttpRequestType.VOLLEY)
                .load();

        callback.executeRequest(connector);
    }

    public void getAll(PaxCallback callback){

        PaxConnector connector = new PaxConnector.Builder()
                .context(context)
                .url(ACHIEVEMENT_EXPLORER_RETRIEVE_REQUEST_URL)
                .classResponse(AchievementResponse.class)
                .methodType(PaxConnector.MethodType.GET)
                .httpRequestType(PaxConnector.HttpRequestType.VOLLEY)
                .load();

        callback.executeRequest(connector);
    }
}
