package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bruno on 22/08/2018.
 */

public class AchievementNew extends BaseModel {

    @JsonProperty("idAchievement")
    private int idAchievement;

    @JsonProperty("nameAchievement")
    private String nameAchievement;

    @JsonProperty("descriptionAchievement")
    private String descriptionAchievement;

    @JsonProperty("quantity")
    private int quantity;

    @JsonProperty("keys")
    private int keys;

    public AchievementNew() {
    }

    public int getIdAchievement() {
        return idAchievement;
    }

    public void setIdAchievement(int idAchievement) {
        this.idAchievement = idAchievement;
    }

    public String getNameAchievement() {
        return nameAchievement;
    }

    public void setNameAchievement(String nameAchievement) {
        this.nameAchievement = nameAchievement;
    }

    public String getDescriptionAchievement() {
        return descriptionAchievement;
    }

    public void setDescriptionAchievement(String descriptionAchievement) {
        this.descriptionAchievement = descriptionAchievement;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getKeys() {
        return keys;
    }

    public void setKeys(int keys) {
        this.keys = keys;
    }
}
