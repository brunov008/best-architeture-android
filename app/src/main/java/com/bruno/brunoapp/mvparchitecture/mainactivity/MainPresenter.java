package com.bruno.brunoapp.mvparchitecture.mainactivity;

import com.bruno.brunoapp.mvparchitecture.core.layer.presenter.BasePresenter;
import com.bruno.brunoapp.mvparchitecture.models.AchievementNew;
import com.bruno.brunoapp.mvparchitecture.models.AchievementResponse;
import com.bruno.brunoapp.mvparchitecture.models.OAuth;
import com.bruno.brunoapp.mvparchitecture.models.User;
import com.example.framework.PaxCallback;
import com.example.framework.utils.PaxUtils;

import java.util.List;

/**
 * Created by bruno on 29/11/17.
 */

public class MainPresenter extends BasePresenter<MainView, MainModel> {

    public MainPresenter(MainView view, MainModel model) {
        super(view, model);
    }

    /*
    void requestToken(User user) {
        PaxUtils.showLoading(context, "Carregando");

        PaxCallback<OAuth> callback = new PaxCallback<OAuth>() {
            @Override
            public void onSuccess(OAuth response) {
                PaxUtils.hideLoading();
                mView.onSuccess(response);
            }

            @Override
            public void onError(String error) {
                PaxUtils.hideLoading();
                PaxUtils.showDialog(context, error, PaxUtils.ERROR);
            }
        };

        mModel.requestToken(callback, user);
    }
    */

    void gellAchievements(){
        PaxUtils.showLoading(context, "Carregando");

        PaxCallback<AchievementResponse> callback = new PaxCallback<AchievementResponse>() {
            @Override
            public void onSuccess(AchievementResponse response) {
                PaxUtils.hideLoading();
                List<AchievementNew> list = response.getListAchievements();
                mView.onListDelivered(list);
            }

            @Override
            public void onError(String error) {
                PaxUtils.hideLoading();
            }
        };

        mModel.getAll(callback);
    }
}
