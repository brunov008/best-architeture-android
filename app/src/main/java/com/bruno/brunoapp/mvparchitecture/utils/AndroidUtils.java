package com.bruno.brunoapp.mvparchitecture.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.bruno.brunoapp.mvparchitecture.models.User;

import java.util.Map;

/**
 * Created by bruno on 29/11/17.
 */

public class AndroidUtils {

    public static void setTicketing(User user, Map<String, String> mHeaders, Context context) {
        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.DEVICE_ID.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.DEVICE_ID.getCode(), getAndroidId(context));
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.MODEL.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.MODEL.getCode(), getModel());
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.ROOT.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.ROOT.getCode(), String.valueOf(isRooted(context)));
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.VERSION.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.VERSION.getCode(), getVersion());
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.SIM_CARD.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.SIM_CARD.getCode(), getSimCard(context));
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.LOCATION.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.LOCATION.getCode(), "-1,-1");
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.BILLING_ID.getCode())) {
            if (!TextUtils.isEmpty(user.getCpf())) {
                mHeaders.put(HeaderTicketingTypeEnum.BILLING_ID.getCode(), user.getCpf());
            } else {
                mHeaders.put(HeaderTicketingTypeEnum.BILLING_ID.getCode(), "");
            }
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.SYSTEM_NAME.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.SYSTEM_NAME.getCode(), "appGreenCard");
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.COMPANY_NAME.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.COMPANY_NAME.getCode(), "GreenCard");
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.PRODUCT_NAME.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.PRODUCT_NAME.getCode(), "appGreenCard");
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.APP_VERSION.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.APP_VERSION.getCode(), "12312421512");
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.ENVIRONMENT_NAME.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.ENVIRONMENT_NAME.getCode(), "prd");
        }

        if (!mHeaders.containsKey(HeaderTicketingTypeEnum.OS_NAME.getCode())) {
            mHeaders.put(HeaderTicketingTypeEnum.OS_NAME.getCode(), "android");
        }
    }

    @SuppressLint("HardwareIds")
    public static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @SuppressLint("HardwareIds")
    private static String getSimCard(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
        }

        String serialNumber = "";
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null) {
            serialNumber = manager.getSubscriberId();
        }
        return serialNumber;
    }

    public static String getModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        return manufacturer + " - " + model;
    }

    public static String getVersion() {
        return Build.VERSION.RELEASE;
    }

    private static boolean isRooted(Context context) {
        return false;
    }
}
