package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 29/11/17.
 */

public class OAuth2Exception extends BaseModel {

    @JsonProperty("error")
    private String mError;

    @JsonProperty("error_description")
    private String mErrorDescription;

    public String getError() {
        return mError;
    }

    public void setError(String error) {
        mError = error;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        mErrorDescription = errorDescription;
    }
}
