package com.bruno.brunoapp.mvparchitecture.core.data;

import android.content.Context;

import com.bruno.brunoapp.mvparchitecture.core.application.dagger.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by bruno on 11/12/17.
 */

@Singleton
public class DataManager {

    SharedPreferencesHelper sharedPreferencesHelper;
    Context context;

    @Inject
    public DataManager(@ApplicationContext Context context,
                       SharedPreferencesHelper sharedPreferencesHelper) {
        this.sharedPreferencesHelper = sharedPreferencesHelper;
        this.context = context;
    }

    public void saveAccessToken(String accessToken) {
        sharedPreferencesHelper.put(SharedPreferencesHelper.PREF_KEY_ACCESS_TOKEN, accessToken);
    }

    public String getAccessToken(){
        return sharedPreferencesHelper.get(SharedPreferencesHelper.PREF_KEY_ACCESS_TOKEN, null);
    }
}
