package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 29/11/17.
 */

public class OAuthAccessToken extends BaseModel {

    @JsonProperty("access_token")
    private String mAccessToken;

    @JsonProperty("token_type")
    private String mTokenType;

    @JsonProperty("refresh_token")
    private String mRefreshToken;

    @JsonProperty("expires_in")
    private String mExpiresIn;

    @JsonProperty("scope")
    private String mScope;

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getTokenType() {
        return mTokenType;
    }

    public void setTokenType(String tokenType) {
        mTokenType = tokenType;
    }

    public String getRefreshToken() {
        return mRefreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        mRefreshToken = refreshToken;
    }

    public String getExpiresIn() {
        return mExpiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        mExpiresIn = expiresIn;
    }

    public String getScope() {
        return mScope;
    }

    public void setScope(String scope) {
        mScope = scope;
    }
}
