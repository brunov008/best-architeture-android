package com.bruno.brunoapp.mvparchitecture.core.application;

import android.app.Application;
import android.content.Context;

import com.bruno.brunoapp.mvparchitecture.core.application.dagger.ApplicationComponent;
import com.bruno.brunoapp.mvparchitecture.core.application.dagger.ApplicationModule;
import com.bruno.brunoapp.mvparchitecture.core.application.dagger.DaggerApplicationComponent;
import com.bruno.brunoapp.mvparchitecture.core.data.DataManager;

import javax.inject.Inject;

/**
 * Created by bruno on 29/11/17.
 */

public class AppController extends Application{

    ApplicationComponent applicationComponent;

    @Inject
    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    public static AppController getInstance(Context context) {
        return (AppController) context.getApplicationContext();
    }

    public ApplicationComponent getComponent(){
        return applicationComponent;
    }
}
