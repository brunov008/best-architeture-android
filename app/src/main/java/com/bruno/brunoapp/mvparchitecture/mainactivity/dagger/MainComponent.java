package com.bruno.brunoapp.mvparchitecture.mainactivity.dagger;


import com.bruno.brunoapp.mvparchitecture.core.application.dagger.ApplicationComponent;
import com.bruno.brunoapp.mvparchitecture.core.application.dagger.PerActivity;
import com.bruno.brunoapp.mvparchitecture.mainactivity.MainActivity;

import dagger.Component;

/**
 * Created by bruno on 29/11/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MainModule.class)
public interface MainComponent {

    void inject (MainActivity activity);
}
