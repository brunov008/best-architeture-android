package com.bruno.brunoapp.mvparchitecture.mainactivity;


import com.bruno.brunoapp.mvparchitecture.core.layer.view.BaseView;
import com.bruno.brunoapp.mvparchitecture.models.AchievementNew;
import com.bruno.brunoapp.mvparchitecture.models.OAuth;

import java.util.List;

/**
 * Created by bruno on 29/11/17.
 */

public interface MainView extends BaseView {
    void onSuccess(OAuth response);
    void onListDelivered(List<AchievementNew> list);
}
