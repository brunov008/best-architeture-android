package com.bruno.brunoapp.mvparchitecture.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 29/11/17.
 */

public class User extends BaseModel {

    @JsonProperty("cpf")
    private String mCpf;

    @JsonProperty("senha")
    private String mPassword;

    public User(String mCpf, String mPassword) {
        this.mCpf = mCpf;
        this.mPassword = mPassword;
    }

    public String getCpf() {
        return mCpf;
    }

    public void setCpf(String mCpf) {
        this.mCpf = mCpf;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }
}
