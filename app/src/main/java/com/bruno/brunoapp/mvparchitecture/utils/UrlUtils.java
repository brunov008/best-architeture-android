package com.bruno.brunoapp.mvparchitecture.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by bruno on 29/11/17.
 */

public class UrlUtils {

    private static final char URL_PARAMETERS_PREFIX = '?';
    private static final char URL_KEY_VALUE_SEPARATOR = '=';
    private static final char URL_KEY_PREFIX = '&';
    private static final String PARAMS_ENCODING = "UTF-8";

    public static String encodeParameters(Map<String, String> params, boolean useParamsPrefix) {

        if (params == null) {
            return null;
        }

        StringBuilder encodedParams = new StringBuilder();
        if (useParamsPrefix) {
            encodedParams.append(URL_PARAMETERS_PREFIX);
        }

        try {

            for (Map.Entry<String, String> entry : params.entrySet()) {
                encodedParams.append(URLEncoder.encode(entry.getKey(), PARAMS_ENCODING));
                encodedParams.append(URL_KEY_VALUE_SEPARATOR);
                encodedParams.append(URLEncoder.encode(entry.getValue(), PARAMS_ENCODING));
                encodedParams.append(URL_KEY_PREFIX);

            }

            return removeAmpersand(encodedParams);
        } catch (UnsupportedEncodingException uee) {
//            Log.e(TAG, uee.getMessage(), uee);
            return null;
        }
    }

    private static String removeAmpersand(StringBuilder encodedParams) {

        String returning = "";
        if(encodedParams.toString().endsWith(String.valueOf(URL_KEY_PREFIX))){
            int length = encodedParams.toString().length();
            returning = encodedParams.substring(0,length-1);
        }
        return returning;
    }
}
